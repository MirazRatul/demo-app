import './App.css';
import View from './components/View';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <View/>
      </header>
    </div>
  );
}

export default App;
